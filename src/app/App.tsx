import { FC } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Alerts } from 'pickit-components';
import '../styles.scss';
import Home from './pages/Home/Home';
import '../config/api';

const App: FC = () => (
  <>
    <Router>
      <Route path="/" exact component={Home} />
    </Router>
    <Alerts />
  </>
);

export default App;
