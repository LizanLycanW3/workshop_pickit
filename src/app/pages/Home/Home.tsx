import { useCallback, FC, ChangeEvent, useState } from 'react';
import {
  SimpleButton,
  $alerts,
  TextInput,
  AutoComplete,
  GlobalLoader,
  NumericInput,
  Checkbox,
  RadioButton,
} from 'pickit-components';
import { Field, Form } from 'react-final-form';
import { HelloProps } from './types';
import styles from './styles.module.scss';
import { useTheming } from '../../../config/theme';
import logoW3 from '../../assets/ico-pickit-naranja.svg';

export const Home: FC<HelloProps> = ({ isActive }) => {
  const [theme, changeTheme] = useTheming();
  const [radio, setRadio] = useState(true);
  // const [open, setOpen] = useState(false);

  const handleThemeChange = useCallback(
    (event: ChangeEvent<HTMLSelectElement>) => {
      changeTheme(event.target.value);
    },
    [changeTheme],
  );

  const changeRadio = useCallback(() => {
    setRadio((currRadio) => !currRadio);
  }, []);

  const openModal = useCallback(() => {
    $alerts.next({
      type: 'error',
      title: 'Warning',
    });
  }, []);

  const onSuccess = useCallback((message) => {
    alert(message);
  }, []);

  const onSubmit = useCallback(
    () =>
      new Promise((resolve) => {
        resolve('SUCCESS');
      }),
    [],
  );

  return (
    <>
      <GlobalLoader isLoading />
      <div className={styles.homeContainer}>
        <img src={logoW3} alt="Logo" />
        <div className={styles.optionsExamples}>
          {isActive}
          <div className={styles.container}>
            <h1>Container with 1000px width</h1>
          </div>
          <div className={styles.containerFluid}>
            <h1>Container fluid [PICKIT COMPONENTS]</h1>
            <div className={styles.containerRow}>
              <div
                className={`${styles.containerColXs6} ${styles.containerColMd4} ${styles.containerColLg2}`}
              >
                <div>
                  COL - LG - 2, COL MD - 4, COL - XS - 6
                  <SimpleButton
                    classes={[styles.mainButton]}
                    label="Main Button"
                    type="button"
                    buttonColor="orange"
                    onClick={openModal}
                  />
                </div>
              </div>
            </div>
            <div className={styles.containerRow}>
              <div className={styles.containerCol12}>
                <div />
              </div>
            </div>

            <Form resetOnSubmit onSuccess={onSuccess} onSubmit={onSubmit}>
              {({ handleSubmit, isSubmitting, valid }: any) => (
                <form className={styles.noteForm} onSubmit={handleSubmit}>
                  <div className={styles.containerRow}>
                    <div
                      className={`${styles.containerCol12} ${styles.containerColMd6} ${styles.containerColLg4}`}
                    >
                      <Field
                        name="textInput"
                        component={TextInput}
                        placeholder="Text Input"
                      />
                    </div>
                    <div
                      className={`${styles.containerCol12} ${styles.containerColMd6} ${styles.containerColLg4}`}
                    >
                      <Field
                        name="numericInput"
                        component={NumericInput}
                        placeholder="Numeric Input"
                      />
                    </div>
                    <div
                      className={`${styles.containerCol12} ${styles.containerColMd6} ${styles.containerColLg4}`}
                    >
                      <Field
                        name="dropdown"
                        component={AutoComplete}
                        placeholder="Dropdown"
                        dropDownOnly
                        options={[
                          {
                            id: 0,
                            name: 'Option 0',
                          },
                          {
                            id: 1,
                            name: 'Option 1',
                          },
                        ]}
                      />
                    </div>
                    <div
                      className={`${styles.containerCol12} ${styles.containerColMd6} ${styles.containerColLg4}`}
                    >
                      <Field
                        name="autocomplete"
                        component={AutoComplete}
                        placeholder="Autocomplete"
                        options={[
                          {
                            id: 0,
                            name: 'Option 0',
                          },
                          {
                            id: 1,
                            name: 'Option 1',
                          },
                        ]}
                      />
                    </div>
                    <div
                      className={`${styles.containerCol12} ${styles.containerColMd6} ${styles.containerColLg4}`}
                    >
                      <Field
                        name="checkbox"
                        component={Checkbox}
                        label="Checkbox"
                      />
                    </div>
                    <div
                      className={`${styles.containerCol12} ${styles.containerColMd6} ${styles.containerColLg4}`}
                    >
                      <RadioButton
                        label="Radio"
                        value={radio}
                        onChange={changeRadio}
                        checked={radio}
                      />
                    </div>
                    <div className={`${styles.containerColXs12}`}>
                      <SimpleButton
                        type="submit"
                        buttonColor="orange"
                        label="Label"
                        disabled={isSubmitting || !valid}
                      />
                    </div>
                  </div>
                </form>
              )}
            </Form>

            <div className={styles.containerRow}>
              <div className={styles.containerCol4}>
                <div>Col 4</div>
              </div>
              <div className={styles.containerCol4}>
                <div>Col 4</div>
              </div>
              <div className={styles.containerCol4}>
                <div>Col 4</div>
              </div>
            </div>

            <div className={styles.containerRow}>
              <div className={styles.containerCol3}>
                <div>Col 3</div>
              </div>
              <div className={styles.containerCol3}>
                <div>Col 3</div>
              </div>
              <div className={styles.containerCol3}>
                <div>Col 3</div>
              </div>
              <div className={styles.containerCol3}>
                <div>Col 3</div>
              </div>
            </div>

            <div className={styles.containerRow}>
              <div
                className={`${styles.containerCol3} ${styles.containerColXs6} ${styles.containerColSm4}`}
              >
                <div>Col XS 12 SM 6</div>
              </div>
              <div
                className={`${styles.containerCol3} ${styles.containerColXs6} ${styles.containerColSm4}`}
              >
                <div>Col XS 12 SM 6</div>
              </div>
              <div
                className={`${styles.containerCol3} ${styles.containerColXs6} ${styles.containerColSm4}`}
              >
                <div>Col XS 12 SM 6</div>
              </div>
              <div
                className={`${styles.containerCol3} ${styles.containerColXs6} ${styles.containerColSm4}`}
              >
                <div>Col XS 12 SM 6</div>
              </div>
              {/* <div className={styles.containerCol3}>Col 3</div>
          <div className={styles.containerCol3}>Col 3</div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
