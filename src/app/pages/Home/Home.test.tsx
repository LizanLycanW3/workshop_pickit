import * as React from 'react';
import { render, fireEvent } from '../../../utils/tests';
import { Home } from './Home';
import themes from '../../../constants/themes';

const setup = (props: any = null) => render(<Home {...props} />);

test('Render Button', async () => {
  const component = setup();
  const button = await component.findByTestId('home-id');
  expect(button).toBeTruthy();
});

test('Should change word hello with world', async () => {
  const component = setup();
  const button = await component.findByTestId('home-id');
  expect(button.textContent).toBe('Hello');
  fireEvent.click(button);
  expect(button.textContent).toBe('world');
});

test('Should change the theme with the select', async () => {
  const component = setup();
  const appliedTheme = document.getElementsByTagName('html')[0].className;
  expect(appliedTheme).toBe(themes.default);
  const select = await component.findByTestId('theme-select');
  fireEvent.change(select, { target: { value: themes.dark } });
  const appliedDarkTheme = document.getElementsByTagName('html')[0].className;
  expect(appliedDarkTheme).toBe(themes.dark);
});

test('On hover should change color', async () => {
  const component = setup();
  const button = await component.findByTestId('home-id');
  expect(button.style.backgroundColor).toBe('rgb(0, 0, 0)');
  fireEvent.mouseEnter(button);
  expect(button.style.backgroundColor).toBe('rgb(255, 0, 0)');
});
